#!/usr/bin/env python3

#    WaRPiG
#    Copyright (C) 2018  Porucznik

#    This program is free software; you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation; version 2 of the License.

#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.

#    You should have received a copy of the GNU General Public License along
#    with this program; if not, write to the Free Software Foundation, Inc.,
#    51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.

import json

def importLevel(x=0, y=0, z=0):
    json_path = 'data/levels/' + str(x) + '.' + str(y) + '.' + str(z) + '.json'
    json_file = open(json_path, 'r')
    json_data = json.load(json_file)
    json_file.close()
    print(json_data)

importLevel()
